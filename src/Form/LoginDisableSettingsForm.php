<?php

namespace Drupal\login_disable\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;

/**
 * Class LoginDisableSettingsForm.
 *
 * @package Drupal\login_disable\Form
 */
class LoginDisableSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_disable_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['login_disable.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('login_disable.settings');

    $form['login_disable_is_active'] = [
      '#type' => 'checkbox',
      '#title' => 'Prevent user log in',
      '#description' => $this->t('When active the user login form will be disabled for everyone. For roles granted bypass rights they must use the access key defined below.'),
      '#default_value' => (bool) $config->get('login_disable_is_active'),
    ];

    $form['login_disable_key'] = [
      '#title' => $this->t('Access key (optional)'),
      '#description' => $this->t('For added security, a word can be required to be added to the URL.'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $config->get('login_disable_key'),
    ];
    if (!empty($form['login_disable_key']['#default_value'])) {
      $form['login_disable_key']['#description'] .= '<br />' . $this->t('The URL to use to log in is: @url', [
        '@url' => Url::fromRoute('user.login')->toString() . '?' . $form['login_disable_key']['#default_value'],
      ]);
    }

    $form['login_disable_message'] = [
      '#title' => $this->t('End-user message when login is disabled'),
      '#description' => $this->t('Adding this word to the end of the @url url will allow access to the log in form.', [
        '@url' => 'user/login?' . $config->get('login_disable_key'),
      ]),
      '#type' => 'textfield',
      '#size' => 80,
      '#default_value' => $config->get('login_disable_message'),
    ];

    $form['login_disable_force_logout'] = [
      '#type' => 'checkbox',
      '#title' => 'Force logout logged in users',
      '#description' => $this->t('When active all the logged in users will be forced to logout.'),
      '#default_value' => (bool) $config->get('login_disable_force_logout'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('login_disable.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    // If user is prevented from login and force logout is selected then logout
    // all users.
    if ($form_state->getValue('login_disable_is_active', FALSE) && $form_state->getValue('login_disable_force_logout', FALSE)) {
      // Ignore super admin and current user from session kill.
      $ignore_uids = array_unique([1, \Drupal::currentUser()->id()]);
      \Drupal::database()
        ->delete('sessions')
        ->condition('uid', $ignore_uids, 'NOT IN')
        ->execute();
    }

    parent::submitForm($form, $form_state);
  }

}
